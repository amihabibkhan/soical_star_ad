<?php

namespace App\Http\Controllers;

use App\Visitor;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($type = 'owner')
    {
        $visitors = Visitor::where('type', 2)->paginate(16);
        if ($type == 'owner'){
            $visitors = Visitor::where('type', 1)->paginate(16);
        }
        return view('home', compact('visitors', 'type'));
    }
}
