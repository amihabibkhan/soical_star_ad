<?php

namespace App\Http\Controllers;

use App\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VisitorController extends Controller
{
    // form page view
    public function form($type = 'owner')
    {
        return view('frontend.form', compact('type'));
    }

    // form submit
    public function formSubmit(Request $request)
    {
        $request->validate([
           'name' => 'required',
           'country' => 'required',
           'district' => 'required',
           'email' => 'required',
           'fb' => 'required',
           'mobile' => 'required',
        ]);
        if($request->type ==1){
            $request->validate([
                'image'=> 'required'
            ]);
        }
        $path = '';
        if ($request->has('image')){
            $path = $request->file('image')->store('product_image');
        }
        Visitor::create([
            'type' => $request->type,
            'name' => $request->name,
            'country' => $request->country,
            'district' => $request->district,
            'email' => $request->email,
            'fb' => $request->fb,
            'mobile' => $request->mobile,
            'imo' => $request->imo,
            'whatsapp' => $request->whatsapp,
            'product_image' => $path,
            'area_of_ad' => $request->area_of_ad,
        ]);

        return back()->with('success', "Registration Successful");
    }

    // delete visitor
    public function deleteVisitor($id)
    {
        if (Visitor::findOrFail($id)->product_image){
            Storage::delete(Visitor::findOrFail($id)->product_image);
        }
        Visitor::findOrFail($id)->delete();
        return back()->with('success', "Visitor Deleted Successfully");
    }
}
