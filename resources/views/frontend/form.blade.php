<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <title>Sign Up for Social Star Ad</title>
    <link rel="stylesheet" href="{{ asset('css/normalize.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

</head>

<body>

@if(session('success'))
    <script>
        Swal.fire(
            'Good job!',
            'Successfully Registered',
            'success'
        )
    </script>
@endif

<form action="{{ route('formSubmit') }}" method="post" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="type" value="{{ ($type == 'owner') ? 1 : 2 }}">
    <fieldset>
        @if($type == 'owner')
        <legend>Sign Up as Product Owner</legend>
        @else
            <legend>Sign Up as Ad Model</legend>
        @endif
        <section class="inputLabel">
            <input type="text" name="name" required onkeyup="this.setAttribute('value', this.value)">
            <label>Name</label>
        </section>
        <section class="inputLabel">
            <input type="text" name="country" required onkeyup="this.setAttribute('value', this.value)">
            <label>Country</label>
        </section>
        <section class="inputLabel">
            <input type="text" name="district" required onkeyup="this.setAttribute('value', this.value)">
            <label>District/Province</label>
        </section>
        <section class="inputLabel">
            <input type="email" name="email" required onkeyup="this.setAttribute('value', this.value)">
            <label>Email</label>
        </section>
        <section class="inputLabel">
            <input type="text" name="fb" required onkeyup="this.setAttribute('value', this.value)">
            <label>Facebook</label>
        </section>
        <section class="inputLabel">
            <input type="text" name="mobile" required onkeyup="this.setAttribute('value', this.value)">
            <label>Mobile</label>
        </section>
        <section class="inputLabel">
            <input type="text" name="imo" onkeyup="this.setAttribute('value', this.value)">
            <label>IMO</label>
        </section>
        <section class="inputLabel">
            <input type="text" name="whatsapp" onkeyup="this.setAttribute('value', this.value)">
            <label>Whatsapp</label>
        </section>
        @if($type == 'owner')
        <section class="inputLabel">
            <span style="color: #fff; margin-bottom: 10px; display: block">Product Image*</span>
            <input type="file" name="image" style="height: auto; " onkeyup="this.setAttribute('value', this.value)">
        </section>
        @endif

        <section>
            <select name="area_of_ad" required>
                <option disabled selected>Select Ad Area</option>
                <option value="facebook">Facebook</option>
                <option value="twitter">Twitter</option>
                <option value="youtube">Youtube</option>
            </select>
        </section>
        <section>
            <input type="submit" value="Sign Up">
        </section>
        <section style="text-align: center">
            @if($type == 'owner')
                <a href="{{ route('form', 'model') }}" style="color: #fff">Sign Up as Ad Model</a>
            @else
                <a href="{{ route('form', 'owner') }}" style="color: #fff">Sign Up as Product Owner</a>
            @endif
        </section>
    </fieldset>
</form>
<script  src="{{ asset('js/index.js') }}"></script>

</body>

</html>
