@extends('layouts.app')

@section('content')
<div class="container">

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    @if(session('success'))
        <script>
            Swal.fire(
                'Good job!',
                '{{ session('success') }}',
                'success'
            )
        </script>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>{{ ($type == 'owner') ? "Product Owner List" : "Ad Model List" }}</h2>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-bordered text-center">
                        <tr>
                            <th>Name</th>
                            <th>Country</th>
                            <th>E-mail</th>
                            <th>Phone</th>
                            <th>Area of Ad</th>
                            <th>Action</th>
                        </tr>
                        @forelse($visitors as $visitor)
                            <tr>
                                <td>{{ $visitor->name }}</td>
                                <td>{{ $visitor->country }}</td>
                                <td>{{ $visitor->email }}</td>
                                <td>{{ $visitor->mobile }}</td>
                                <td>{{ $visitor->area_of_ad }}</td>
                                <td>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#view{{ $visitor->id }}">
                                        View
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="view{{ $visitor->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalCenterTitle">Visitor's Details Info</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-bordered table-striped">
                                                        <tr>
                                                            <th>Name</th>
                                                            <td>{{ $visitor->name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Country</th>
                                                            <td>{{ $visitor->country }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>District/Province</th>
                                                            <td>{{ $visitor->district }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>E-mail</th>
                                                            <td>{{ $visitor->email }}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>Facebook</th>
                                                            <td><a target="_blank" href="{{ $visitor->fb }}">Click to see</a></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Phone number</th>
                                                            <td>{{ $visitor->mobile }}</td>
                                                        </tr>
                                                        @if($visitor->imo)
                                                        <tr>
                                                            <th>Imo</th>
                                                            <td>{{ $visitor->imo }}</td>
                                                        </tr>
                                                        @endif
                                                        @if($visitor->whatsapp)
                                                        <tr>
                                                            <th>Whatsapp</th>
                                                            <td>{{ $visitor->whatsapp }}</td>
                                                        </tr>
                                                        @endif
                                                        @if($visitor->product_image)
                                                        <tr>
                                                            <th>Product Image</th>
                                                            <td><img src="{{ asset('/storage') }}/{{ $visitor->product_image }}" class="img-fluid" alt=""></td>
                                                        </tr>
                                                        @endif
                                                        <tr>
                                                            <th>Ad Area</th>
                                                            <td>{{ strtoupper($visitor->area_of_ad )}}</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- delete button --}}
                                    <a href="{{ route('deleteVisitor', $visitor->id) }}" class="btn btn-danger" onclick="return confirm('Are you sure to Delete?')">Delete</a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No Data Found</td>
                            </tr>
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
