<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home/{type?}', 'HomeController@index')->name('home');

Route::get('/{type?}', 'VisitorController@form')->name('form');
Route::post('/form/submit', 'VisitorController@formSubmit')->name('formSubmit');
Route::get('/visitor/delete/{id}', 'VisitorController@deleteVisitor')->name('deleteVisitor');
